FROM java:8 as builder

WORKDIR /app

ADD build.gradle .
ADD gradle.properties .
ADD gradlew .
ADD gradle/ gradle

# For caching of faster builds
RUN ./gradlew

ADD src/ src
RUN ./gradlew build

FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim

EXPOSE 8080

COPY --from=builder /app/build/libs/*-all.jar app.jar
CMD java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Dcom.sun.management.jmxremote -noverify ${JAVA_OPTS} -jar app.jar
